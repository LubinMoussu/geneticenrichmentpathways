import os
import unittest

import app
from app.enrichment import SetEnrichment


class TestEnrichment(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.reference_filepath = os.path.join(
            app.references_sets_dir, "sets_uniprot.txt"
        )
        candidate_filename = "test_genes.txt"
        cls.candidate_filepath = os.path.join(
            app.candidates_dir,
            candidate_filename,
        )
        cls.enrichment_filepath = os.path.join(
            app.enrichments_dir,
            candidate_filename,
        )

    def setUp(self) -> None:
        """:arg"""
        self.path_set_enrichment = SetEnrichment()
        self.path_set_enrichment.load_reference_set()
        self.path_set_enrichment.load_gene_set()

    def test_pipeline(self):
        self.path_set_enrichment.pipeline(
            self.enrichment_filepath,
        )
