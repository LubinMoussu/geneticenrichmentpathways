from scipy.stats import binom

from .compared_set import ComparedSet
from .identifiers import Identifiers
from .reference_set import ReferenceSet


class SetEnrichment:
    _out_format = "{0}\t{1}\t{2}/{3}\t{4}\t{5}\n"
    _pvalue_threshold = 0.05
    reference_set = None
    identifiers = None
    enrichment_analysis = None
    filtered_enrichment_analysis = None

    def load_reference_set(self, filename: str) -> None:
        self.reference_set = ReferenceSet(filename=filename)
        self.reference_set.load()

    def load_gene_set(self, filename: str) -> None:
        self.identifiers = Identifiers()
        self.identifiers.load(filename)

    def evaluate_set(self, set_id: str) -> ComparedSet:
        """
        binomial test
        null hypothesis : name distribution in the testing set is the same in the reference set
        p-value corresponds to the probability to have the same distribution by picking randomly elts
        into the reference set.
        :param set_id:
        :return:
        """
        elements = self.reference_set.sets[set_id]["gene_products"]
        name = self.reference_set.sets[set_id]["function"]
        common_elements = set(elements).intersection(
            self.identifiers.ids,
        )
        pval = binom.cdf(
            self.identifiers.ids_size - len(common_elements),
            self.identifiers.ids_size,
            1 - float(len(elements)) / self.reference_set.population_size,
        )
        return ComparedSet(
            set_id,
            name,
            len(common_elements),
            len(elements),
            pval,
            elements,
            common_elements,
        )

    def evaluate_multiple_set(self) -> None:
        enrichment_analysis = map(
            self.evaluate_set,
            list(self.reference_set.sets.keys()),
        )
        self.enrichment_analysis = [elt for elt in enrichment_analysis]

    def filter_multiple_set(self) -> None:
        """
        keep ComparedSet with pvalue < self._pvalue_threshold
        :return:
        """

        filtered_enrichment_analysis = filter(
            lambda x: x.pvalue < self._pvalue_threshold,
            self.enrichment_analysis,
        )
        self.filtered_enrichment_analysis = [
            elt for elt in filtered_enrichment_analysis
        ]

    def sort_multiple_set(self) -> None:
        """
        sort ComparedSet by pvalue
        :return:
        """
        self.filtered_enrichment_analysis.sort(key=lambda x: x.pvalue)

    def write_enrichment_analysis(self, fout: str) -> None:
        """
        write down filtered ComparedSets
        """
        with open(fout, "w") as f:
            for obj in self.filtered_enrichment_analysis:
                f.write(
                    self._out_format.format(
                        obj.id,
                        round(obj.pvalue, 5),
                        obj.common,
                        obj.size,
                        obj.name,
                        ", ".join(obj.common_elements),
                    ),
                )

    def pipeline(self, reference_filename: str, gene_filename: str, fout: str):
        self.load_reference_set(reference_filename)
        self.load_gene_set(gene_filename)
        self.evaluate_multiple_set()
        self.filter_multiple_set()
        self.sort_multiple_set()
        self.write_enrichment_analysis(fout)
