class ReferenceSet:
    """
    load GO terms assigned to list of genes

    """

    def __init__(self, filename: str):
        self.filename = filename
        self.sets = {}
        self.gene_distribution = {}
        self.population_size = None

    def load(self) -> None:

        self.sets = {}
        self.gene_distribution = {}
        with open(self.filename) as f:
            content = f.read()
            lines = content.split("\n")
            for line in lines:
                words = line.split("\t")
                if len(words) > 2 and not words[0].startswith("#"):
                    go_id = words.pop(0)
                    name = words.pop(0)
                    self.sets[go_id] = {
                        "function": name,
                        "gene_products": words,
                    }
                    for word in words:
                        try:
                            self.gene_distribution[word] += 1
                        except KeyError:
                            self.gene_distribution[word] = 1

        self.population_size = len(self.gene_distribution.keys())
