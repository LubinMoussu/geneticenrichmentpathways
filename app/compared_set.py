class ComparedSet:
    def __init__(
        self,
        id,
        name="",
        common=0,
        size=0,
        pvalue=1,
        elements=None,
        common_elements=None,
    ):
        if common_elements is None:
            common_elements = []
        if elements is None:
            elements = []
        self.id = id
        self.name = name
        self.common = common
        self.size = size
        self.pvalue = pvalue
        self.elements = elements
        self.common_elements = common_elements
