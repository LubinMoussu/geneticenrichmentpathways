from os.path import isfile


class Identifiers:
    _delimiter = "\n"

    def __init__(self):
        self.ids = []
        self.ids_size = None

    def load(self, text):
        if isfile(text):
            with open(text) as f:
                content = f.read()
                lines = content.split(self._delimiter)
                for line in lines:
                    if line != "":
                        words = line.split()
                        for word in words:
                            if word not in self.ids:
                                self.ids.append(word)
        else:  # parse string
            words = text.split()
            for word in words:
                if word not in self.ids:
                    self.ids.append(word)
        self.ids_size = len(self.ids)
