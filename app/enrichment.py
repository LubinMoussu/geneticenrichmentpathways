from scipy.stats import binom

from .compared_set import ComparedSet
from .identifiers import Identifiers
from .reference_set import ReferenceSet
import pandas as pd
import random

services = ["HTTP", "FTP", "SSH", "DNS", "SMTP", "IMAP", "POP3", "SNMP", "LDAP", "NTP"]
hosts = [
    "Server1",
    "Server2",
    "Server3",
    "Server4",
    "Server5",
    "Server6",
    "Server7",
    "Server8",
    "Server9",
    "Server10",
]


class SetEnrichment:
    _out_format = "{0}\t{1}\t{2}/{3}\t{4}\t{5}\n"
    _pvalue_threshold = 0.05
    reference_set = None
    identifiers = None
    enrichment_analysis = None
    filtered_enrichment_analysis = None
    df = None

    def load_reference_set(self) -> pd.DataFrame:
        data = {
            "products": [random.choice(services) for _ in range(50)],
            "sets": [random.choice(hosts) for _ in range(50)],
        }

        return pd.DataFrame(data)

    def load_gene_set(self) -> None:
        self.identifiers = ["HTTP", "FTP", "SSH"]

    def evaluate_set(self, set_id: str) -> ComparedSet:
        """
        binomial test
        null hypothesis : name distribution in the testing set is the same in the reference set
        p-value corresponds to the probability to have the same distribution by picking randomly elts
        into the reference set.
        :param set_id:
        :return:
        """
        elements = self.df[self.df.sets == set_id].products.to_list()
        common_elements = set(elements).intersection(
            self.identifiers,
        )
        name = ""
        pval = binom.cdf(
            len(self.identifiers) - len(common_elements),
            len(self.identifiers),
            1 - float(len(elements)) / self.df.shape[0],
        )
        return ComparedSet(
            set_id,
            name,
            len(common_elements),
            len(elements),
            pval,
            elements,
            common_elements,
        )

    def evaluate_multiple_set(self) -> None:
        # enrichment_analysis = map(
        #     self.evaluate_set, list(self.reference_set.sets.keys()),
        # )
        # self.enrichment_analysis = [elt for elt in enrichment_analysis]
        self.enrichment_analysis = [
            self.evaluate_set(set_id) for set_id in self.df.sets.unique()
        ]

    def filter_multiple_set(self) -> None:
        """
        keep ComparedSet with pvalue < self._pvalue_threshold
        :return:
        """

        filtered_enrichment_analysis = filter(
            lambda x: x.pvalue < self._pvalue_threshold,
            self.enrichment_analysis,
        )
        self.filtered_enrichment_analysis = [
            elt for elt in filtered_enrichment_analysis
        ]

    def sort_multiple_set(self) -> None:
        """
        sort ComparedSet by pvalue
        :return:
        """
        self.filtered_enrichment_analysis.sort(key=lambda x: x.pvalue)

    def write_enrichment_analysis(self, fout: str) -> None:
        """
        write down filtered ComparedSets
        """
        with open(fout, "w") as f:
            for obj in self.filtered_enrichment_analysis:
                f.write(
                    self._out_format.format(
                        obj.id,
                        round(obj.pvalue, 5),
                        obj.common,
                        obj.size,
                        obj.name,
                        ", ".join(obj.common_elements),
                    ),
                )

    def pipeline(self, fout: str):
        self.df = self.load_reference_set()
        self.load_gene_set()
        self.evaluate_multiple_set()
        self.filter_multiple_set()
        self.sort_multiple_set()
        self.write_enrichment_analysis(fout)
