import os

APP_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.dirname(APP_DIR)
DATA_DIR = os.path.join(ROOT_DIR, "data")

references_sets_dir = os.path.join(DATA_DIR, "references_sets")
candidates_dir = os.path.join(DATA_DIR, "candidates")
enrichments_dir = os.path.join(DATA_DIR, "enrichments")
